﻿namespace Coscine.Api.Resources.ParameterObjects;

/// <summary>
///  Parameter object containing the update information.
/// </summary>    
public class UpdateResourceObject
{
    /// <summary>
    /// New reserved quota value to set for the selected resource in GiB.
    /// </summary>
    public int ReservedGiB { get; set; }
}
