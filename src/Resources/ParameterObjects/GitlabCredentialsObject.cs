﻿using System;

namespace Coscine.Api.Resources.ParameterObjects;

/// <summary>
///  Parameter object containing the gitlab credentials information.
/// </summary>
public class GitlabCredentialsObject
{
    /// <summary>
    /// Domain/Host of the GitLab Provider
    /// </summary>
    /// <example>https://git.rwth-aachen.de/</example>
    public Uri Domain { get; set; }
    /// <summary>
    /// GitLab Project or Group Access Token
    /// </summary>
    public string AccessToken { get; set; } = null!;
}
