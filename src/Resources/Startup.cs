﻿using Coscine.ApiCommons;

namespace Coscine.Api.Resources
{
    /// <summary>
    /// Standard Startup class.
    /// </summary>
    public class Startup : AbstractStartup
    {
        /// <summary>
        /// Standard Startup constructor.
        /// </summary>
        public Startup()
        {
        }
    }
}