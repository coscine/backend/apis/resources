﻿using Coscine.ApiCommons;
using Coscine.Database.DataModel;
using Coscine.Database.Models;
using Coscine.ResourceTypes;
using Coscine.ResourceTypes.Base.Models;
using Coscine.ResourceTypes.Base.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Coscine.Api.Resources.Controllers
{
    /// <summary>
    /// This controller represents the actions which can be taken with a resource type object.
    /// </summary>
    [Authorize]
    public class ResourceTypeController : Controller
    {
        private readonly ResourceTypeModel _resourceTypeModel;
        private readonly ProjectQuotaModel _projectQuotaModel;

        private readonly Authenticator _authenticator;

        /// <summary>
        /// ResourceTypeController constructor specifying a ResourceTypeModel.
        /// </summary>
        public ResourceTypeController()
        {
            _resourceTypeModel = new();
            _projectQuotaModel = new ProjectQuotaModel();
            _authenticator = new(this, Program.Configuration);
        }

        /// <summary>
        /// Returns all resource types. Both hidden and active, regardless of user affiliation or quota.
        /// </summary>
        /// <returns>List of ResourceTypeInformation for all resources.</returns>
        [Route("[controller]/types")]
        public async Task<ActionResult<IEnumerable<ResourceTypeInformation>>> GetResourceTypes()
        {
            var listOfSpecificResourceTypes = ResourceTypeFactory.Instance.GetSpecificResourceTypes().Select(x => x.SpecificTypeName);

            var resourceTypes = _resourceTypeModel.GetAllWhere((resourceType) => listOfSpecificResourceTypes.Contains(resourceType.SpecificType));
            return Json(await CreateListOfResourceTypeInformation(resourceTypes));
        }

        /// <summary>
        /// Returns all enabled resource types according to user's affiliation and allocated quota in  the project.
        /// </summary>
        /// <param name="projectId">Id of the project</param>
        /// <returns>List of ResourceTypeInformations for the enabled Resources</returns>
        [Route("[controller]/types/{projectId}/-/enabled")]
        public async Task<ActionResult<IEnumerable<ResourceTypeInformation>>> GetEnabledResourceTypes(Guid projectId)
        {
            var user = _authenticator.GetUser();

            var orgs = Util.OrganizationsHelper.GetOrganization(user).ToList();

            return Json(await CreateListOfResourceTypeInformation(Util.ResourceTypeHelper.GetAvailableResourceTypesForProject(projectId, orgs)));
        }

        /// <summary>
        /// Returns all enabled resource types.
        /// </summary>
        /// <returns>List of ResourceTypeInformations for the enabled Resources</returns>
        [Route("[controller]/types/-/enabled")]
        public async Task<ActionResult<IEnumerable<ResourceTypeInformation>>> GetEnabledResourceTypes()
        {
            var user = _authenticator.GetUser();

            var orgs = Util.OrganizationsHelper.GetOrganization(user).ToList();

            if (orgs?.Any() != true)
            {
                orgs = new List<string> { "*" };
            }

            var listOfActiveResources = orgs.SelectMany(x => ResourceTypeFactory
                .Instance
                .GetSpecificResourceTypes(x, ResourceTypes.Base.ResourceTypeStatus.Active)
                .Select(x => x.SpecificTypeName));

            var allEnabledResourceTypes = _resourceTypeModel.GetAllWhere((resourceType) => listOfActiveResources.Contains(resourceType.SpecificType));
            return Json(await CreateListOfResourceTypeInformation(allEnabledResourceTypes));
        }

        /// <summary>
        /// Returns all fields of the specified resource type.
        /// </summary>
        /// <param name="id">A GUID as a string that identifies the resource.</param>
        /// <returns>ResourceTypeInformation</returns>
        [Route("[controller]/types/{id}")]
        public async Task<ActionResult<ResourceTypeInformation>> GetResourceType(string id)
        {
            var resourceType = _resourceTypeModel.GetById(Guid.Parse(id));
            return Json(await CreateResourceTypeInformation(resourceType.Id, resourceType.Type, resourceType.SpecificType));
        }

        private static async Task<ResourceTypeInformation> CreateResourceTypeInformation(Guid id, string type, string specificType)
        {
            var resourceTypeObject = ResourceTypeFactory.Instance.GetResourceType(type, specificType);
            if (resourceTypeObject == null)
            {
                return null;
            }

            var resourceTypeInformation = await resourceTypeObject.GetResourceTypeInformation();
            resourceTypeInformation.Id = id;

            return resourceTypeInformation;
        }

        private static async Task<List<ResourceTypeInformation>> CreateListOfResourceTypeInformation(IEnumerable<ResourceType> resourceTypeList)
        {
            var resourceTypeInformationList = new List<ResourceTypeInformation>();
            foreach (var resourceType in resourceTypeList)
            {
                var resourceTypeInformation = await CreateResourceTypeInformation(resourceType.Id, resourceType.Type, resourceType.SpecificType);
                if (resourceTypeInformation != null)
                {
                    resourceTypeInformationList.Add(resourceTypeInformation);
                }
            }
            return resourceTypeInformationList;
        }
    }
}