﻿using Coscine.Database.DataModel;
using Coscine.Database.Models;
using Coscine.ResourceTypes;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Coscine.Api.Resources.Util
{
    internal static class ResourceTypeHelper
    {
        private static readonly ResourceTypeModel _resourceTypeModel = new();
        private static readonly ProjectQuotaModel _projectQuotaModel = new();

        internal static IEnumerable<ResourceType> GetAvailableResourceTypesForProject(Guid projectId, IEnumerable<string> affiliatedOrganizaions)
        {
            if (affiliatedOrganizaions?.Any() != true)
            {
                affiliatedOrganizaions = new List<string> { "*" };
            }

            var listOfAffiliatedResources = affiliatedOrganizaions.SelectMany(x => ResourceTypeFactory
                .Instance
                .GetSpecificResourceTypes(x, ResourceTypes.Base.ResourceTypeStatus.Active)
                .Select(x => x.SpecificTypeName));

            var listOfEnabledResources = listOfAffiliatedResources;

            foreach (var resourceType in ResourceTypeFactory
                .Instance
                .GetSpecificResourceTypes(ResourceTypes.Base.ResourceTypeStatus.Active)
                .Select(x => x.SpecificTypeName))
            {
                // check if the resource type is not in the list of affiliated resource types and if so, if the project has reserved maxQuota for this resource type
                if (!listOfAffiliatedResources.Contains(resourceType))
                {
                    var resourceId = _resourceTypeModel.GetAllWhere(x => x.SpecificType == resourceType).FirstOrDefault().Id;
                    var quotas = _projectQuotaModel.GetAllWhere(x => x.ProjectId == projectId && x.ResourceTypeId == resourceId);
                    if (quotas.Any() && quotas.First().MaxQuota > 0)
                    {
                        listOfEnabledResources = listOfEnabledResources.Append(resourceType).ToList();
                    }
                }
            }

            return _resourceTypeModel.GetAllWhere((resourceType) => listOfEnabledResources.Contains(resourceType.SpecificType));
        }

        internal static bool IsResourceTypeUsable(ResourceType resourceType, Guid projectId, IEnumerable<string> affiliatedOrganizations)
        {
            return resourceType.Enabled == true && GetAvailableResourceTypesForProject(projectId, affiliatedOrganizations)
                .Select(x => x.Id)
                .Contains(resourceType.Id);
        }
    }
}